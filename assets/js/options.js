function save_options() {
	TagCashBot.saveState("maxPage", $("#maxpagecount").val());

	$('#status').html('<p class="alert alert-success">Options Saved</p>');
		setTimeout(function () {
			$('#status').html('');
		}, 3500);
}

function restore_options() {
	var maxPage = TagCashBot.getState().maxPage;

	if (maxPage) {
		$("#maxpagecount").val(maxPage);
	}
}

$(document).ready(function () {
    restore_options();
    $('#btnsave').click(save_options);
});