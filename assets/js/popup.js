(function(window, jQuery){

    $(document).ready(function(){

    	chrome.extension.sendMessage({
    		msg: 'status'
    	}, function(response) {
    		if (response.status == "started") {
    			if (response.botMode) {
    				$("div.retailer-search").hide();
    				$("div.bot").show();
    				$("#stop").show();
    			} else {
    				$("div.bot").hide();
    				$("div.retailer-search").show();
    				$("#search-stop").show();

    				if ((response.contacts) && (response.contacts.length > 0)) {
    					$("div.retailer-search #download").show();
    				}
    			}
    		} else {
    			$("div.bot").show();
    			$("div.bot #start").show();
    			$("div.retailer-search").show();
    			$("div.retailer-search #search-start").show();
    		}

            if ((response.contacts) && (response.contacts.length > 0)) {
                $("div.retailer-search #download").show();
            }

            if ((response.bot_contacts) && (response.bot_contacts.length > 0)) {
                $("div.bot #bot-download").show();
            }

    		if (response.lead) {
    			$("#download").attr("download", response.lead.name + ".csv");
    		}
    	});

        $("#start").click(function(event) {
            event.preventDefault();
            chrome.extension.sendMessage({msg: "bot-start"}, function(response) {
                console.log("Response from background script: " + response.data);
                if (response.data === "Started") {
                    $("#start").hide();
                    $("#stop").show();
                } else if (response.data == "no_lead") {
                    $("#stop").hide();
                    $("div.bot").show();
                    $("div.bot #start").show();
                    $("div.retailer-search").show();
                    $("div.retailer-search #search-start").show();
                }
            });
        });

        $("#stop").click(function(event) {
            event.preventDefault();
            chrome.extension.sendMessage({msg: "bot-stop"}, function(response) {
                console.log("Response from background script: " + response.data);
                $("#stop").hide();
                $("div.bot").show();
    			$("div.bot #start").show();
    			$("div.retailer-search").show();
    			$("div.retailer-search #search-start").show();

    			chrome.extension.sendMessage({
						msg: 'status'
					}, function(response) {
						if ((response.contacts) && (response.contacts.length > 0)) {
							$("div.retailer-search #download").show();
						}
					});
            });
        });

        $("#search-start").click(function(event) {
        	var lead = $('#retailer').val(),
        		source = $("#source").val();
        	event.preventDefault();

        	if (!lead) {
        		alert("Input lead name.", function() {
        			$("#retailer").focus();
        		});
        	} else if (!source) {
        		alert("Source network should be selected.");
        	} else {
        		chrome.extension.sendMessage({msg: "search-start", retailer: lead, source_network: source}, function(response) {
	                console.log("Response from background script: " + response.data);
	                $("#search-start").hide();
	                $("#search-stop").show();
	            });
        	}
        });

        $("#search-stop").click(function(event) {
            event.preventDefault();
            chrome.extension.sendMessage({msg: "search-stop"}, function(response) {
                console.log("Response from background script: " + response.data);
                $("#search-stop").hide();
                $("div.bot").show();
    			$("div.bot #start").show();
    			$("div.retailer-search").show();
    			$("div.retailer-search #search-start").show();

    			chrome.extension.sendMessage({
						msg: 'status'
					}, function(response) {
						if ((response.contacts) && (response.contacts.length > 0)) {
							$("div.retailer-search #download").show();
						}
					});
            });
        });
        var x = function(flag, obj){
                return function(){
                    downloadContacts(flag, obj);
                }
        };

        $("#download").click(x("non-bot", $("#download")));
        $("#bot-download").click(x("bot", $("#bot-download")));
    });


})(window, $);