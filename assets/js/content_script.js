chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
        if (request.msg == "isInjected")
            sendResponse({message: true});
});

(function(window, jQuery) {
    var lookbookSearchUrl = "http://lookbook.nu/search?",
        poseSearchUrl = "https://pose.com/search/poses",
        chictopiaUrl = "http://www.chictopia.com/",
        lookbookPage = 1,
        lookbookItemCount = 0,
        posePage = 1,
        poseItemCount = 0,
        chictopiaPage = 0,
        chictopiaItemCount = 0,
        chictopiaLinks = [],
        maxPageLimit = 1;

    var displayAllLookbook = function(url) {
        if (maxPageLimit == 0) {
            $.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                success: function(res) {
                    var payload = res.payload;
                    if (payload != " ") {
                        lookbookPage ++;
                        processLookbookItems($(payload));
                        displayAllLookbook(res.next_url);
                    } else {
                        stopLookbookCrawl();
                    }
                },
                error: function(x, e) {
                    stopLookbookCrawl();
                }
            });
        } else if (lookbookPage > maxPageLimit) {
            stopLookbookCrawl();
        } else {
            $.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                success: function(res) {
                    var payload = res.payload;
                    if (payload != " ") {
                        lookbookPage ++;
                        processLookbookItems($(payload));
                        displayAllLookbook(res.next_url);
                    } else {
                        stopLookbookCrawl();
                    }
                },
                error: function(x, e) {
                    stopLookbookCrawl();
                }
            });
        }
    };

    var displayAllPose = function(url, page) {
        if (maxPageLimit == 0) {
            $.ajax({
                url: url + '&page=' + page,
                type: "GET",
                dataType: "json",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("X-CSRF-Token", "uwfeFTQ43on2/nRwfqFcKwu1ymvS0D0k/JD/ruxQZA4=");
                    xhr.setRequestHeader("X-NewRelic-ID", "VgcBUF9SGwEAVVNUDwQ=");
                },
                success: function(res) {
                    var poses = res.results;
                    if (poses.length > 0) {
                        posePage++;
                        processPoseItems(poses);
                        displayAllPose(url, page + 1);
                    } else {
                        stopPoseCrawl();
                    }
                },
                error: function(x, e) {
                    stopPoseCrawl();
                }
            });
        } else if (posePage > maxPageLimit) {
                stopPoseCrawl();
        } else {
            $.ajax({
                url: url + '&page=' + page,
                type: "GET",
                dataType: "json",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("X-CSRF-Token", "uwfeFTQ43on2/nRwfqFcKwu1ymvS0D0k/JD/ruxQZA4=");
                    xhr.setRequestHeader("X-NewRelic-ID", "VgcBUF9SGwEAVVNUDwQ=");
                },
                success: function(res) {
                    var poses = res.results;
                    if (poses.length > 0) {
                        posePage++;
                        processPoseItems(poses);
                        displayAllPose(url, page + 1);
                    } else {
                        stopPoseCrawl();
                    }
                },
                error: function(x, e) {
                    stopPoseCrawl();
                }
            });
        }
    };

    var processLookbookItems = function($items) {
        var urls = [];

        $links = $items.find("a.light") || [];
        for (var i = 0; i < $links.length; i++) {
            var curItem = $links[i];
            urls.push(curItem.href);
        }
        //urls = unique(urls);
        lookbookItemCount += urls.length;


        sendMessage("lookbook_list", {data: urls});
    };

    var processPoseItems = function(items) {
        var urls = [];

        for (var i = 0; i < items.length; i++) {
            urls.push(items[i].creator.url);
        }

        //urls = unique(urls);
        poseItemCount += urls.length;

        sendMessage("pose_list", {data: urls});
    };

    var processChitopiaItems = function(items) {
        var urls = [];

        for (var i = 0; i < items.length; i++) {
            if ($(items[i]).find("a")[0])
                urls.push($(items[i]).find("a")[0].href);
        }
        //urls = unique(urls);

        //var diff = $(urls).not(chictopiaLinks).get();

        chictopiaItemCount += urls.length;
        chictopiaPage++;
        chictopiaLinks = chictopiaLinks.concat(urls)

        sendMessage("chictopia_list", {data: urls});
    };

    var crawlLookbook = function() {
        //
        lookbookItemCount = 0;
        lookbookPage = 1;

        items = $('#search_container li.minilook');
        processLookbookItems(items);

        displayAllLookbook(window.location.href + '&page=2');
    };

    var stopLookbookCrawl = function() {
        sendMessage("lookbook_close", {count: lookbookItemCount, page: lookbookPage});
    };

    var crawlPose = function() {
        var keyword = window.location.href.substr(window.location.href.indexOf("keyword=") + 8);
        displayAllPose('https://pose.com/items/search.json?page_size=12&keyword=' + keyword, 1);
    };

    var crawlChictopia = function() {
        var path = window.location.pathname.substr(1);
        var keyword = path.substring(0, path.indexOf("/"));

        var $items = $("div.lg_photo.photo_hover div.info_overlay .ellipsis.bold.px12");
        processChitopiaItems($items);
        displayAllChictopia(window.location.href + "/", 2);
    };

    var displayAllChictopia = function(url, page) {
        if (maxPageLimit == 0) {
            $.ajax({
                url: url + page + '?scrollpage=1',
                type: "GET",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("X-CSRF-Token", "emhn55VdPEqFBT6aMGHi9dKz6zJYksFAhcOb4JVjN8s=");
                },
                complete: function(xhr, status) {
                    if (status === 'error' || !xhr.responseText) {
                        console.log("error occurs");
                    } else {
                        var data = parseChictopiaResponseText(xhr.responseText);
                        eval(data);

                        var $next = $("div.lg_photo.photo_hover div.info_overlay .ellipsis.bold.px12");
                        processChitopiaItems($next);//.not($prev).get());
                        displayAllChictopia(window.location.href + "/", page + 1);
                    }
                }
            });
        } else if (page > maxPageLimit) {
            stopChictopiaCrawl();
        } else {
            $.ajax({
                url: url + page + '?scrollpage=1',
                type: "GET",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("X-CSRF-Token", "emhn55VdPEqFBT6aMGHi9dKz6zJYksFAhcOb4JVjN8s=");
                },
                complete: function(xhr, status) {
                    if (status === 'error' || !xhr.responseText) {
                        console.log("error occurs");
                    } else {
                        var data = parseChictopiaResponseText(xhr.responseText);
                        eval(data);

                        var $next = $("div.lg_photo.photo_hover div.info_overlay .ellipsis.bold.px12");
                        processChitopiaItems($next);//.not($prev).get());
                        displayAllChictopia(window.location.href + "/", page + 1);
                    }
                }
            });
        }
    };

    var parseChictopiaResponseText = function(rawText) {
        var suffix = "chicAjaxPageLoader.setLoadOnScroll(true);",
            pos = rawText.indexOf(suffix);

        return rawText.substring(0, pos);
    };

    var stopPoseCrawl = function() {
        sendMessage("pose_close", {count: poseItemCount, page: posePage});
    };

    var stopChictopiaCrawl = function() {
        sendMessage("chictopia_close", {count: chictopiaItemCount, page: chictopiaPage});
    };

    var extractPersonalInfoFromLookbook = function() {
        var blogUrl = ($("[data-page-track*='blog url click']")[0] || $("[data-page-track*='website url click']")[0] || {}).href,
            location = ($("a[data-page-track*='country click |']") || {}).text(),
            email = ($("#userabout a[href*='mailto:']") || {}).text();

        sendMessage("lookbook", {blog: blogUrl, location: location, email: email});
    };

    var extractPersonalInfoFromPose = function() {
        var blogUrl = ($("#profile div.meta p.website a")[0] || {}).href;
        sendMessage("pose", {blog: blogUrl});
    };

    var extractPersonalInfoFromChictopia = function() {
        var blogUrl = ($("#right_column div[itemprop='author'] > div a.ullink")[0] || {}).href,
            location = $("#right_column div[itemprop='author'] div#loc_div > a").text();
        sendMessage("chictopia", {blog: blogUrl, location: location});
    };

    var extractFollowerFromInstagram = function() {
        var follower = ($($("div.UserProfileUserInfo ul.upuiStats li span.sCount")[1]) || {}).text();

        if (follower)
            sendMessage("instagram", {url: window.location.href, follower: follower});
        else {
            sendMessage("instagram", {url: window.location.href, follower: "0"});
        }
    };

    var extractFollowerFromTwitter = function() {
        var follower = ($("a[data-nav='followers'] .ProfileNav-value") || {}).text();

        if (follower)
            sendMessage("twitter", {url: window.location.href, follower: follower});
        else {
            sendMessage("twitter", {url: window.location.href, follower: "0"});
        }
    };

    var extractFollowerFromFacebook = function() {
        var pref = 'Followed by ',
            tmp = $("#PagesLikesCountDOMID span").text(),
            follower = "";
        if (!tmp) {
            bodyText = $('body').text();

            if (bodyText.indexOf(pref) > -1) {
                bodyText = bodyText.substr(bodyText.indexOf(pref) + pref.length);
                follower = bodyText.substring(0, bodyText.indexOf(" "));
            } else {
                console.log("facebook likes count not found.");
            }
        } else {
            follower = tmp.substring(0, tmp.indexOf(" "));
        }

        if (follower)
            sendMessage("facebook", {url: window.location.href, follower: follower});
        else {
            sendMessage("facebook", {url: window.location.href, follower: "0"});
        }
    };

    var extractSocialLinksFromBlog = function() {
        //
        var facebookLink = ($("a[href*='https://facebook.com/']")[0] || $("a[href*='http://facebook.com/']")[0] || $("a[href*='https://www.facebook.com/']")[0] || $("a[href*='http://www.facebook.com/']")[0] || {}).href,
            twitterLink = ($("a[href*='https://twitter.com/']")[0] || $("a[href*='http://twitter.com/']")[0] || $("a[href*='https://www.twitter.com/']")[0] || $("a[href*='http://www.twitter.com/']")[0] || {}).href,
            instagramLink = ($("a[href*='https://instagram.com/']")[0] || $("a[href*='http://instagram.com/']")[0] || $("a[href*='https://www.instagram.com/']")[0] || $("a[href*='http://www.instagram.com/']")[0] || {}).href;

        sendMessage("blog", {facebook: facebookLink, twitter: twitterLink, instagram: instagramLink});
    };

    var isChictopiaSearchUrl = function(addr) {
        if (addr.indexOf(chictopiaUrl) == 0) {
            addr = addr.substr(chictopiaUrl.length);
            slashPos = addr.indexOf("/");
            addr = addr.substr(slashPos + 1);
            return (addr == "info");
        } else {
            return false;
        }
    };
    $(document).ready(function() {

        chrome.extension.sendMessage({
                msg: 'status'
            }, function(response) {
                if (response.status == "started") {
                    var curDomain = window.location.origin + window.location.pathname,
                        curTabId = null;

                    maxPageLimit = parseInt(response.maxPage);

                    sendMessage("current_tab", {}, function(result) {
                        curTabId = result.tabId;

                        sendMessage("query", {field: "blog"}, function(res) {
                            if (compareUrl(window.location.href, res.value) || (curTabId == res.tabId)) {
                                extractSocialLinksFromBlog(res.contact);
                            } else {
                                if (window.location.href.indexOf(lookbookSearchUrl) == 0) {
                                    crawlLookbook();
                                } else if (window.location.href.indexOf(poseSearchUrl) == 0) {
                                    crawlPose();
                                } else if (isChictopiaSearchUrl(window.location.href) ){
                                    crawlChictopia();
                                } else if (window.location.origin.indexOf("http://lookbook.nu") != -1) {
                                    sendMessage("query", {field: "source_network_url"}, function(res) {
                                        if (compareUrl(window.location.href, res.value) || (curTabId == res.tabId)) {
                                            extractPersonalInfoFromLookbook();
                                        }
                                    });
                                } else if (window.location.origin.indexOf("https://pose.com") == 0) {
                                    sendMessage("query", {field: "source_network_url"}, function(res) {
                                        //
                                        if (comparePoseUrl(window.location.href, res.value)  || (curTabId == res.tabId)) {
                                            extractPersonalInfoFromPose();
                                        }
                                    });
                                } else if (window.location.origin.indexOf("http://www.chictopia.com") == 0) {
                                    sendMessage("query", {field: "source_network_url"}, function(res) {
                                        //
                                        if (compareUrl(window.location.href, res.value)  || (curTabId == res.tabId)) {
                                            extractPersonalInfoFromChictopia();
                                        }
                                    });
                                } else if (window.location.origin.indexOf("https://instagram.com") != -1) {
                                    sendMessage("query", {field: "instagram"}, function(res){
                                        if (compareUrl(window.location.href, res.value)  || (curTabId == res.tabId))
                                            extractFollowerFromInstagram();
                                    });
                                } else if (window.location.origin.indexOf("https://twitter.com") != -1) {
                                    sendMessage("query", {field: "twitter"}, function(res){
                                        if (compareUrl(window.location.href, res.value)  || (curTabId == res.tabId))
                                            extractFollowerFromTwitter();
                                    });
                                } else if (window.location.origin.indexOf("https://www.facebook.com") != -1) {
                                    sendMessage("query", {field: "facebook"}, function(res){
                                        if (compareUrl(window.location.href, res.value)  || (curTabId == res.tabId))
                                            extractFollowerFromFacebook();
                                    });
                                } 
                            }
                        });
                    });
                }
            });
                    
    });
})(window, $)