(function(window, jQuery){
	chrome.extension.onMessage.addListener(function(request, sender, sendResponse) {

		if (request.msg == "bot-start") {
			console.log("App is starting...");
			// sendResponse({data: "Started"});
			TagCashBot.saveState('botMode', true);
			TagCashBot.start(sendResponse);
		} else if (request.msg == "search-start") {
			console.log("App is starting...");
			sendResponse({data: "Started"});
			TagCashBot.startSearch(request.retailer, request.source_network);
		} else if (request.msg == "status") {
			console.log("Checking the current status.");
			sendResponse({
				status: TagCashBot.getState().status,
				botMode: TagCashBot.getState().botMode,
				contacts: TagCashBot.getState().contacts,
				bot_contacts: TagCashBot.getState("bot-contacts"),
				lead: TagCashBot.getState().lead,
				maxPage: TagCashBot.getState().maxPage
			});
		}

		if (TagCashBot.getState().status == "started") {

			if (request.msg == "bot-stop") {
				console.log("App is stopping...");
				TagCashBot.stop();
				sendResponse({data: "Stopped"});
			} else if (request.msg == "search-stop") {
				console.log("Search utility is stopping...");
				TagCashBot.stop();
				sendResponse({data: "stopped"});
			}

			else if (request.msg == "lookbook_list") {
				console.log("Lookbook items..");
				links = request.data.data;
				TagCashBot.addLookbookItems(links);
			} 

			else if (request.msg == "pose_list") {
				console.log("Pose items added..");
				links = request.data.data;
				TagCashBot.addPoseItems(links);
			} 

			else if (request.msg == "chictopia_list") {
				console.log("Chictopia items added..");
				links = request.data.data;
				TagCashBot.addChictopiaItems(links);
			} 

			else if (request.msg == "lookbook") {

				var data = request.data;
				if (data.blog)
					TagCashBot.contact.blog = data.blog;

				if (data.location)
					TagCashBot.contact.country = data.location;

				if (data.instagram)
					TagCashBot.contact.instagram = data.instagram;

				if (data.email)
					TagCashBot.contact.email = data.email;

				removeChromeTab(sender.tab.id);
				TagCashBot.scrapContactInfo();

			} 

			else if (request.msg == "lookbook_close") {
				removeChromeTab(sender.tab.id);
				//TagCashBot.lookbook_tab_id = null;
			}

			else if (request.msg == "pose_close") {
				removeChromeTab(sender.tab.id);
				//TagCashBot.pose_tab_id = null;
			}

			else if (request.msg == "chictopia_close") {
				removeChromeTab(sender.tab.id);
				//TagCashBot.chictopia_tab_id = null;
			}

			else if (request.msg == "pose") {

				var data = request.data;
				if (data.blog)
					TagCashBot.contact.blog = data.blog;

				removeChromeTab(sender.tab.id);
				TagCashBot.scrapContactInfo();
			} 

			else if (request.msg == "chictopia") {

				var data = request.data;
				if (data.blog)
					TagCashBot.contact.blog = data.blog;

				if (data.location)
					TagCashBot.contact.country;

				removeChromeTab(sender.tab.id);
				TagCashBot.scrapContactInfo();
			} 

			else if (request.msg == "query") {

				var field = request.data.field;

				if (!field)
					sendResponse({});

				if (field == "source_network_url") {
					sendResponse({value: TagCashBot.source_network_url, tabId: TagCashBot.source_network_url});
				} else if (field == "instagram") {
					if (TagCashBot.contact)
						sendResponse({value: TagCashBot.contact.instagram, tabId: TagCashBot.instagram_tab_id});
					else
						sendResponse({value: "none", tabId: TagCashBot.instagram_tab_id});
				} else if (field == "twitter") {
					if (TagCashBot.contact)
						sendResponse({value: TagCashBot.contact.twitter, tabId: TagCashBot.twitter_tab_id});
					else
						sendResponse({value: "none", tabId: TagCashBot.twitter_tab_id});
				} else if (field == "facebook") {
					if (TagCashBot.contact)
						sendResponse({value: TagCashBot.contact.facebook, tabId: TagCashBot.facebook_tab_id});
					else 
						sendResponse({value: "none", tabId: TagCashBot.facebook_tab_id});
				} else if (field == "blog") {
					if (TagCashBot.contact)
						sendResponse({value: TagCashBot.contact.blog, tabId: TagCashBot.blog_tab_id, contact: TagCashBot.contact});
					else
						sendResponse({value: "none", tabId: TagCashBot.blog_tab_id, contact: {}});
				}
			} 

			else if (request.msg == "instagram") {
				if (request.data.follower) {
					TagCashBot.contact.instagram_count = request.data.follower || "0";

					if (TagCashBot.instagram_tab_id) {
						clearTimeout(TagCashBot.instagram_timer);
						removeChromeTab(TagCashBot.instagram_tab_id);
						//TagCashBot.instagram_tab_id = null;
					}
				} else {
					TagCashBot.contact.instagram_count = "0";
				}

				if (TagCashBot.isReady()) {
					TagCashBot.saveContact(function() {
						var curState = TagCashBot.getState();
						if (curState.lookbook && (curState.lookbook != "done")) {
							TagCashBot.processLookbookItem("background.js: msg -> facebook message handler.");
						} else if (curState.pose && (curState.pose != "done")) {
							TagCashBot.processPoseItem("background.js: msg -> facebook message handler.");
						} else if (curState.chictopia && (curState.chictopia != "done")) {
							TagCashBot.processChictopiaItem("background.js: msg -> facebook message handler.");
						} else {
							console.log("Unknown exception occurs in facebook handler.");
							TagCashBot.nextLead();
							console.log("Moving forward to the next lead.");
						}
					});
				}

			} 

			else if (request.msg == "blog") {
				if (request.data) {
					var data = request.data;

					if (data.facebook)
						TagCashBot.contact.facebook = data.facebook;

					if (data.twitter)
						TagCashBot.contact.twitter = data.twitter;

					if (data.instagram)
						TagCashBot.contact.instagram = data.instagram;

					if (TagCashBot.blog_tab_id) {
						clearTimeout(TagCashBot.blog_timer);
						removeChromeTab(TagCashBot.blog_tab_id);
						//TagCashBot.blog_tab_id = null;
					}

					TagCashBot.visitSocialSites();
				}
			} 

			else if (request.msg == "facebook") {
				if (request.data.follower) {
					TagCashBot.contact.facebook_count = request.data.follower || "0";

					if (TagCashBot.facebook_tab_id) {
						clearTimeout(TagCashBot.facebook_timer);
						removeChromeTab(TagCashBot.facebook_tab_id);
						//TagCashBot.facebook_tab_id = null;
					}
				} else {
					TagCashBot.contact.facebook_count = "0"
				}

				if (TagCashBot.isReady()) {
					TagCashBot.saveContact(function() {
						var curState = TagCashBot.getState();
						if (curState.lookbook && (curState.lookbook != "done")) {
							TagCashBot.processLookbookItem("background.js: msg -> facebook message handler.");
						} else if (curState.pose && (curState.pose != "done")) {
							TagCashBot.processPoseItem("background.js: msg -> facebook message handler.");
						} else if (curState.chictopia && (curState.chictopia != "done")) {
							TagCashBot.processChictopiaItem("background.js: msg -> facebook message handler.");
						} else {
							console.log("Unknown exception occurs in facebook handler.");
							TagCashBot.nextLead();
							console.log("Moving forward to the next lead.");
						}
					});
				}
			} 

			else if (request.msg == "twitter") {
				if (request.data.follower) {
					TagCashBot.contact.twitter_count = request.data.follower || "0";

					if (TagCashBot.twitter_tab_id) {
						clearTimeout(TagCashBot.twitter_timer);
						removeChromeTab(TagCashBot.twitter_tab_id);
						//TagCashBot.twitter_tab_id = null;
					}
				} else {
					TagCashBot.contact.twitter_count = "0";
				}

				if (TagCashBot.isReady()) {
					TagCashBot.saveContact(function() {
						var curState = TagCashBot.getState();
						if (curState.lookbook && (curState.lookbook != "done")) {
							TagCashBot.processLookbookItem("background.js: msg -> facebook message handler.");
						} else if (curState.pose && (curState.pose != "done")) {
							TagCashBot.processPoseItem("background.js: msg -> facebook message handler.");
						} else if (curState.chictopia && (curState.chictopia != "done")) {
							TagCashBot.processChictopiaItem("background.js: msg -> facebook message handler.");
						} else {
							console.log("Unknown exception occurs in facebook handler.");
							TagCashBot.nextLead();
							console.log("Moving forward to the next lead.");
						}
					});
				}
			}

			else if (request.msg == "current_tab") {
				sendResponse({tabId: sender.tab.id});
			}
		}
			
	});
})(window, $);